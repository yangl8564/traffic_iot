package org.iot.spark.util

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.serialization.{Deserializer, Serializer}
import org.iot.spark.vo.IotData

object IotDataDecode{
  private val obj_mapper:ObjectMapper = new ObjectMapper();
}

class IotDataDecode extends Deserializer[IotData]{
  override def configure(map: util.Map[String, _], b: Boolean): Unit = {

  }

  override def deserialize(s: String, bytes: Array[Byte]): IotData = {
    try{
      return IotDataDecode.obj_mapper.readValue(bytes,classOf[IotData]);
    }catch{
      case e: Exception => println(e)
    }
    return null;
  }

  override def close(): Unit = {

  }
}
