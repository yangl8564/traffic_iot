package org.iot.spark.enity
import java.util.Date

import com.fasterxml.jackson.annotation.JsonFormat

case class POITrafficData(
    vehicleId:String,
    distance:Double,
    vehicleType:String,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "MST")
    timeStamp:Date
)
