package com.iot.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iot.vo.IotData;
import org.apache.log4j.Logger;

import java.util.Map;
import org.apache.kafka.common.serialization.Serializer;

public class IotDataEncoder implements Serializer<IotData>{

    private static ObjectMapper object_mapper = new ObjectMapper();
    private static final Logger logger = Logger.getLogger(IotDataEncoder.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, IotData data) {
        try {
            String msg = object_mapper.writeValueAsString(data);
            logger.info(msg);
            return msg.getBytes();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void close() {

    }
}
